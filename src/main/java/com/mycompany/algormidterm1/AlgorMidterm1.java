/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.algormidterm1;

import java.util.Scanner;

/**
 *
 * @author user
 */
public class AlgorMidterm1 {

    public static void main(String[] args) {
        Scanner n = new Scanner(System.in);
        int m = n.nextInt(); // รับ input กำหนด size array
        String[] arr1 = new String[m]; //สร้าง array1 เพื่่อรับ input
        String[] arr2 = new String[m]; //สร้าง array2 เพื่อเก็บค่าจาก array1 มา reverse
        int j = m; //กำหนด index ใน array2
        for(int i=0;i<m; i++){
            arr1[i] = n.next();
            System.out.print(arr1[i]+" ");
        }
        
        System.out.println();
        for(int i=0; i<m; i++){
            arr2[j-1] = arr1[i]; // ย้ายตัวที่อยู่ใน array1ตัวที่ i ไปไว้ใน array2 ตัวที่ ่่่j-1
            j = j-1;
        }
        
        for(int i=0; i<m; i++){
            System.out.print(arr2[i]+" "); //print reverse array
        }
        
    }
}
